FROM node:17-slim
# install BWA
RUN apt-get update -y
RUN  apt-get install -y bwa
# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy local code to the container image.
COPY . ./

# Run the web service on container startup.
CMD ["node", "index.js"]
