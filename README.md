# run request
`curl --data-binary @test.fasta http://localhost:8000/bwa`

# parallel requests
`seq 50 | parallel -N0 curl --silent --data-binary @test.fasta https://<APP URL>.run.app/bwa`