const { exec } = require('child_process');

process.on('message', (message) => {
  if (message == 'START') {
    const tempFile = process.argv[2]
    console.log('Child process received START message');
    console.log(`Starting bwa index on ${tempFile}`);
    exec(`bwa index ${tempFile} ; rm ${tempFile}*`, (err, stdout, stderr) => {
      if (err) {
        console.error(`exec error: ${err}`);
        return;
      }
      if (stderr.match(/Real time: ([0-9\.]+) sec/)){
        const realTime = stderr.match(/Real time: ([0-9\.]+) sec/)[1]
        const message = `{"bwa time": "${realTime}"}\n`;
        process.send(message);
      } else {
        const message = `{"bwa error":  ${stderr}}`
        process.send(message);
      }
    });

  }
});