const http = require('http');
const { fork } = require('child_process');
const fs = require('fs');
const crypto = require("crypto");

const port = process.env.PORT || 8080;

const requestListener = function (req, res) {
  if (req.url === '/bwa') {
    const tempFile = __dirname + "/" + crypto.randomBytes(16).toString("hex") + ".fasta";
    // console.log(tempFile)
    const stream = fs.createWriteStream(tempFile);
    req.on('data', function(chunk) {
      stream.write(chunk);
    });
    const child = fork('runBWA', [tempFile]);

    child.on('message', (message) => {
      console.log('Returning /bwa results');
      res.setHeader('Content-Type', 'application/json');
      res.writeHead(200);
      res.end(message);
    });

    child.send('START');
  } else if (req.url === '/hello') {
    console.log('Returning /hello results');
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(`{"message":"hello"}`);
  }
};

const server = http.createServer(requestListener);
server.listen(port, () => {
  console.log(`Server is running on ${port}`);
});